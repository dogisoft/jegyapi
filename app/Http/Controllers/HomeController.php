<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;

class HomeController extends Controller
{
        
    public function index(){        
            
            $city = 0;

            $category = DB::table('category')->distinct()            
            ->where('parent_jid', 0)        
            ->orderBy('name', 'ASC')        
            ->get();     

            $city = DB::table('city')
            ->orderBy('name', 'ASC')
            ->get();
            
            
            $venues = DB::table('venue')
            ->orderBy('name', 'ASC')
            ->get();        
                        
            $items = DB::table('event')
            ->where('EventStatus_Id', 1)        
            ->where('ForForeigners', \Config::get('app.foreigners'))          
            ->orderBy('EventDateTS', 'DESC')
            ->paginate('18'); 
                                 
            $eventnum = DB::table('event')
                    ->where('EventStatus_Id', 1)
                    ->where('ForForeigners', \Config::get('app.foreigners'))                    
                    ->count();             
                
        return view('home', ['events'=>$items, 'category'=>$category, 'cities'=>$city, 'venues'=>$venues, 'eventnum' => $eventnum]);
    }
    
    public function getCitiesByCatID($catid){        
        
        $tomb = explode('sub_cat_id_', $catid);
        $tnum = count($tomb);
        
        if($catid == '0'){
            ///nincs selected
            $items = DB::table('city')
                ->select("name")->distinct()    
                ->orderBy('name')    
                ->get();
                echo "<option value=\"0\">Összes város</option>";
            foreach($items as $kulcs){
                echo "<option>".$kulcs->name."</option>";
            }              
        }
        else{           
            //van selected
            if($tnum>1){
                $miben = "ProgramSubtype_Id";
                $mennyi = $tomb[1];
            }
            else{        
                $miben = "ProgramType_Id";
                $mennyi = $catid;                
            }
            
            $items = DB::table('event')
                ->select("City")->distinct()    
                ->where($miben, $mennyi)
                ->where('ForForeigners', \Config::get('app.foreigners'))                    
                ->orderBy('City')    
                ->get();
                                   
                echo "<option value=\"0\">Összes város</option>";
            foreach($items as $kulcs){
                echo "<option>".$kulcs->City."</option>";
            }            
        }

        
    }
    
    public function getVenues(Request $request){        

        $catid = $request->catid;
        $cityid = $request->citytid;
        
        if($catid=='0'){
            $items = DB::table('event')
                ->select("VenueNameURL", "VenueName")->distinct()    
                ->where('ForForeigners', \Config::get('app.foreigners'))                
                ->where('City', $cityid)    
                ->get();            
        }
        else{
            
        $tomb = explode('sub_cat_id_', $catid);
        $tnum = count($tomb);            
        
            if($tnum>1){
                $miben = "ProgramSubtype_Id";
                $mennyi = $tomb[1];
            }
            else{        
                $miben = "ProgramType_Id";
                $mennyi = $catid;                
            }        
            
            $items = DB::table('event')
                ->select("VenueNameURL", "VenueName")->distinct()    
                ->where('ForForeigners', \Config::get('app.foreigners'))
                ->where($miben, $mennyi)
                ->where('City', $cityid)    
                ->get();            
        }
        
        echo "<option value=\"0\">Összes helyszín</option>";
        
        foreach($items as $kulcs){            
            echo "<option value='".$kulcs->VenueNameURL."'>".$kulcs->VenueName."</option>";
        }
    }
    
    
    public function filter(Request $request){   
        
            $items = DB::table('event')          
            ->orderBy('id', 'DESC')
            ->paginate('18');         
        
            
               $cda = $request->cat;
               $tomb = explode('sub_cat_id_', $request->cat);
               
               if(count($tomb)>1){
                   $ctype = "ProgramSubtype_Id";
                   $cda = $tomb[1];
               }
               else{
                   $ctype = "ProgramType_Id"; 
                   $cda = $request->cat;
               }            
             
            if($request->cat!=='0' && $request->city=='0' && $request->venue=='0'){
               
               $items = DB::table('event') 
               ->where($ctype, $cda)
               ->where('ForForeigners', \Config::get('app.foreigners'))
               ->orderBy('id', 'DESC')
               ->paginate('18');        
               
            }          


            if($request->cat=='0' && $request->city=='0' && $request->venue!=='0'){
               $items = DB::table('event') 
               ->where('VenueNameURL', $request->venue)  
               ->orderBy('id', 'DESC')
               ->where('ForForeigners', \Config::get('app.foreigners'))        
               ->paginate('18');  
               
            }
                  
            if($request->cat=='0' && $request->city!=='0' && $request->venue=='0'){
               $items = DB::table('event') 
               ->where('City', $request->city)  
               ->orderBy('id', 'DESC')
               ->where('ForForeigners', \Config::get('app.foreigners'))        
               ->paginate('18');               
             
            }
            
            if($request->cat!=='0' && $request->city!=='0' && $request->venue!=='0'){
                
               $items = DB::table('event') 
               ->where('City', $request->city)  
               ->where('VenueNameURL', $request->venue)
               ->where($ctype, $cda)       
               ->where('ForForeigners', \Config::get('app.foreigners'))        
               ->orderBy('id', 'DESC')
               ->paginate('18');               
              
            }            
 
            $category = DB::table('category')
            ->where('parent_jid', 0)        
            ->orderBy('name', 'ASC')
            ->get();         

            $city = DB::table('city')
            ->orderBy('name', 'ASC')
            ->get();;         
            
            $venues = DB::table('venue')
            ->orderBy('name', 'ASC')
            ->get();                  
                        
            $eventnum = DB::table('event')
                    ->where('ForForeigners', \Config::get('app.foreigners'))
                    ->count();  
                       

        return view('home', ['cat'=>$request->cat, 'city'=>$request->city, 'venue'=>$request->venue, 'events'=>$items, 'category'=>$category, 'cities'=>$city, 'venues'=>$venues, 'eventnum' => $eventnum]);
    }
    
    
    public function event(Request $request, $slug, $event_id){            

            $category = DB::table('category')
            ->where('parent_jid', 0)        
            ->orderBy('name', 'ASC')
            ->get();         

            $city = DB::table('city')
            ->orderBy('name', 'ASC')
            ->get();
            
            $venues = DB::table('venue')
            ->orderBy('name', 'ASC')
            ->get();          
            
            $eventnum = DB::table('event')
                    ->where('ForForeigners', \Config::get('app.foreigners'))
                    ->count(); 

            $event = DB::table('event')
            ->where('NameURL', $slug)  
            ->where('id', $event_id)
            ->where('ForForeigners', \Config::get('app.foreigners'))        
            ->first(); 
            
        $db = DB::table('event_views')->where('Event_Id', $event->Event_Id)->first();
        
        if(!$db){
          //add
           DB::table('event_views')->insert([
               ['Event_Id' => $event->Event_Id, 'views' => 1]
            ]);            
        }
        else{
            //update
            DB::table('event_views')
                 ->where('Event_Id', $event->Event_Id)
                ->increment('views');             
        }
        
        return view('event', ['views' => $db, 'eventData' => $event, 'category'=>$category, 'cities'=>$city, 'venues'=>$venues, 'eventnum' => $eventnum]);
    }    
    
    
    
    public function search(Request $request){   
                       
        $query = Event::where('ProgramName', 'LIKE', '%'.$request->kereses.'%');
        $query->orWhere('ShortDescription', 'LIKE', '%'.$request->kereses.'%');            
        $res = $query->count();
        $items = $query->paginate(18); 
                                    
            $category = DB::table('category')
            ->where('parent_jid', 0)        
            ->orderBy('name', 'ASC')
            ->get();         

            $city = DB::table('city')
            ->orderBy('name', 'ASC')
            ->get();;         
            
            $venues = DB::table('venue')
            ->orderBy('name', 'ASC')
            ->get();                  
                        
            $eventnum = DB::table('event')
                    ->where('ForForeigners', \Config::get('app.foreigners'))
                    ->count();                           

        return view('home', ['res' => $res, 'kereses' =>$request->kereses, 'events'=>$items, 'category'=>$category, 'cities'=>$city, 'venues'=>$venues, 'eventnum' => $eventnum]);
    }    
    
    public static function getChild($parentid){
        
            $category = DB::table('category')
            ->where('parent_jid', $parentid)        
            ->orderBy('name', 'ASC')
            ->get(); 
            return $category;
    }
    
}