<?php

namespace App\Http\Controllers;
use DB;

class ConsumerController extends Controller
{
    
    
    public function index(){        
        //echo \Config::get('app.siteurl');
        return view('selector');
    }
    
    public function city(){
        
        $data =  $this->CallAPI('POST', ['_function_name'=>'get_city_list', '_format'=>'json']);

        $data = json_decode($data);
        $array = 0;
        
        foreach ($data as $kulcs => $ertek){
            
            $item = DB::table('city')
            ->where('jid', $ertek->id)
            ->value('id');          
            
            if($item){
                DB::table('city')
                ->where('jid', $ertek->id)
                ->update(['jid' => $ertek->id, 'name' => $ertek->name, 'name_url' => $ertek->name_url, 'lat' => $ertek->lat, 'lng' => $ertek->lng, 'created_at' => date('Y-m-d H:i:s')]);            
                
            }
            else{
                DB::table('city')->insert([
                    ['jid' => $ertek->id, 'name' => $ertek->name, 'name_url' => $ertek->name_url, 'lat' => $ertek->lat, 'lng' => $ertek->lng, 'created_at' => date('Y-m-d H:i:s')]
                ]); 
                $msg = "New entries has been created!";
                
            }
            
            
        }
        
       return view('answer');
    }
    
    
    public function category(){
        
        $data =  $this->CallAPI('POST', ['_function_name'=>'get_categories', '_format'=>'json']);
        $data = json_decode($data);        
        $array = 0;        
        DB::table('category')->delete();                
        foreach ($data as $kulcs => $ertek){    
                        
            DB::table('category')->insert([
               ['jid' => $ertek->id, 'parent_jid' => 0, 'name' => $ertek->name, 'name_url' => $ertek->name_url, 'created_at' => date('Y-m-d H:i:s')]
            ]);            
            
            if($ertek->sub_categories){
                foreach ($ertek->sub_categories as $kulcs1 => $ertek1){
                    DB::table('category')->insert([
                        ['jid' => $ertek1->id, 'parent_jid' => $ertek->id, 'name' => $ertek1->name, 'name_url' => $ertek1->name_url, 'created_at' => date('Y-m-d H:i:s')]
                    ]);                 

                }
            }           
            
        }
        
       return view('answer');
    } 
    
    public function venue(){
        
        $data =  $this->CallAPI('POST', ['_function_name'=>'get_venue_list', '_format'=>'json']);
        $data = json_decode($data);  
        $data = $data->venue_list;
 
        $array = 0;        
        DB::table('venue')->delete();        
        
        foreach ($data as $kulcs => $ertek){    
            
            if(count($ertek->images)<1){
                $ertek->images = "empty";
            }
            else{
                $ertek->images = "none";
            }
            
            if(count($ertek->originalImages)<1){
                $ertek->originalImages = "empty";
            }            
            else{
                $ertek->originalImages = "none";
            }
                        
            DB::table('venue')->insert([
               ['jid' => $ertek->id,
                   'name' => $ertek->name,
                   'name_url' => $ertek->name_url,
                   'city' => $ertek->city,
                   'lat' => $ertek->lat,
                   'lng' => $ertek->lng,
                   'images' => $ertek->images,
                   'country' => $ertek->country,
                   'imageLarge' => $ertek->imageLarge,
                   'address' => $ertek->address,
                   'zip' => $ertek->zip,
                   'originalImages' => $ertek->originalImages,
                   'created_at' => date('Y-m-d H:i:s')]
            ]);            
            
        }
        
       return view('answer');
    }     
    
    
    public function event(){
                
        $data =  $this->CallAPI('POST', ['_function_name'=>'get_event_list', '_format'=>'json',  '_categories'=>1, '_version'=>2, '_request' => '{"per_page":1000,"page":0}']);
        $data = json_decode($data);          
        
        $data = $data->payload->Events;             
        
        foreach ($data as $kulcs => $ertek){    
                      
            $elem = DB::table('event')->where('Event_Id', $ertek->Event_Id)->value('Event_Id');           
            
            if(count($elem)>0){   
                DB::table('event')->where('Event_Id', $elem)->update(['NetEvent_Id' => $ertek->NetEvent_Id,
                   'NetProgram_Id' => $ertek->NetProgram_Id,
                   'Venue_Id' => $ertek->Venue_Id,                   
                   'EventStatus_Id' => $ertek->EventStatus_Id,
                   'ProgramType_Id' => $ertek->ProgramType_Id,
                   'ProgramSubtype_Id' => $ertek->ProgramSubtype_Id,
                   'ForForeigners' => $ertek->ForForeigners,
                   'ProgramName' => $ertek->ProgramName,
                   'ShortDescription' => $ertek->ShortDescription,                                     
                   'City' => $ertek->City,
                   'VenueName' => $ertek->VenueName,
                   'NameURL' => $ertek->NameURL,
                   'NameDisplay' => $ertek->NameDisplay,
                   'VenueNameURL' => $ertek->VenueNameURL,
                   'LinkURL' => $ertek->LinkURL,
                   'EventURL' => $ertek->EventURL,
                   'CityURL' => $ertek->CityURL,
                   'ThumbURL' => $ertek->ThumbURL,
                   'EventDateTS' => $ertek->EventDateTS,
                   'EventDateFrom' => $ertek->EventDateFrom,
                   'EventDateTo' => $ertek->EventDateTo,
                   'EventDateFormat' => $ertek->EventDateFormat,
                   'EventDateShort1' => $ertek->EventDateShort1,
                   'EventDateShort2' => $ertek->EventDateShort2,
                   'Image' => $ertek->Image]);
            }
            else{
                
                DB::table('event')->insert([
                   ['NetEvent_Id' => $ertek->NetEvent_Id,
                   'Event_Id' => $ertek->Event_Id,
                   'NetProgram_Id' => $ertek->NetProgram_Id,
                   'Venue_Id' => $ertek->Venue_Id,                   
                   'EventStatus_Id' => $ertek->EventStatus_Id,
                   'ProgramType_Id' => $ertek->ProgramType_Id,
                   'ProgramSubtype_Id' => $ertek->ProgramSubtype_Id,
                   'ForForeigners' => $ertek->ForForeigners,
                   'ProgramName' => $ertek->ProgramName,
                   'ShortDescription' => $ertek->ShortDescription,                                     
                   'City' => $ertek->City,
                   'VenueName' => $ertek->VenueName,
                   'NameURL' => $ertek->NameURL,
                   'NameDisplay' => $ertek->NameDisplay,
                   'VenueNameURL' => $ertek->VenueNameURL,
                   'LinkURL' => $ertek->LinkURL,
                   'EventURL' => $ertek->EventURL,
                   'CityURL' => $ertek->CityURL,
                   'ThumbURL' => $ertek->ThumbURL,
                   'EventDateTS' => $ertek->EventDateTS,
                   'EventDateFrom' => $ertek->EventDateFrom,
                   'EventDateTo' => $ertek->EventDateTo,
                   'EventDateFormat' => $ertek->EventDateFormat,
                   'EventDateShort1' => $ertek->EventDateShort1,
                   'EventDateShort2' => $ertek->EventDateShort2,
                   'Image' => $ertek->Image,
                   'created_at' => date('Y-m-d H:i:s')]
            ]);            
            }            
        }    
       return view('answer');
    }   

    public function CallAPI($method, $data = false)
    {        
        $url = \Config::get('app.siteurl');
        
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "WEBGURU:ZSENI");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }        
    
    
    public function readFiles()
    {
        $file = public_path('event.csv');

        $customerArr = $this->csvToArray($file);

        for ($i = 0; $i < count($customerArr); $i ++)
        {
            print $customerArr[$i]."<br>";
            //User::firstOrCreate($customerArr[$i]);
        }

        return 'Done';    
    }    
    
    
}