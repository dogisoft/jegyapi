<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Event extends Model
{
    //
    protected $table = 'event';
    
    public static function ajanlo(){        
            
            $items = DB::table('event')
            ->where('ajanlo', 1)
            ->orderBy('id', 'DESC')
            ->limit(\Config::get('app.ajanlo_limit'))
            ->get(); 
            
           return $items;                         
        
    }
    
}
