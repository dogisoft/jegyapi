<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/sync', 'ConsumerController@index');
Route::get('/update-city', 'ConsumerController@city');
Route::get('/update-category', 'ConsumerController@category');
Route::get('/update-venue', 'ConsumerController@venue');
Route::get('/update-event', 'ConsumerController@event');

Route::get('/get-cities-by-id/{id}', 'HomeController@getCitiesByCatID');
Route::get('/get-venues', 'HomeController@getVenues');
Route::get('/esemeny/{slug}/{event_id}', 'HomeController@event');
Route::post('/filter', 'HomeController@filter');
Route::get('/filter', 'HomeController@filter');
Route::get('/keres', 'HomeController@search');
Route::get('/read-files', 'ConsumerController@readFiles');