<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jid');	
            $table->string('name', 60);
            $table->string('name_url', 60);
            $table->string('city', 60);
            $table->string('lat', 50);
            $table->string('lng', 50);
            $table->text('images');
            $table->text('originalImages');
            $table->string('imageLarge', 255);
            $table->string('country', 150);
            $table->string('address', 150);
            $table->string('zip', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venue');
    }
}
