<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('NetEvent_Id');
            $table->integer('Event_Id');
            $table->integer('NetProgram_Id');
            $table->integer('Venue_Id');
            $table->integer('EventStatus_Id');
            $table->string('ForForeigners', 5);
            
            $table->string('ProgramName', 50);
            $table->text('ShortDescription');
            $table->string('City', 100);
            $table->string('VenueName', 100);
            $table->string('NameURL', 255);
            $table->string('NameDisplay', 100);
            
            $table->string('VenueNameURL', 255);
            $table->string('LinkURL', 255);
            $table->string('EventURL', 255);
            $table->string('CityURL', 255);

            $table->string('ThumbURL', 255);
            $table->integer('EventDateTS');         
            $table->date('EventDateFrom', 20);  
            $table->date('EventDateTo', 20);  
            
            $table->string('EventDateFormat', 50); 
            $table->string('EventDateShort1', 50); 
            $table->string('EventDateShort2', 50);         
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
