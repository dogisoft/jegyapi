$('.selectpicker').selectpicker();
		
$('.datepicker').datepicker({
	language: 'hu',
	weekStart: 1,
	format: 'yyyy.mm.dd.',
	startDate: '0d',
	autoclose: true,
	/*<input pattern="\d*" inputmode="numeric" readonly -> mobile number */
});

$('#productsTab a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
});

$(window).load(function(){
	$('#basicModal').modal('show');
});

(function($) {
	fakewaffle.responsiveTabs(['xs']);
})(jQuery);

$(function () {
  $('.box-office-tabs .panel-group').on('shown.bs.collapse', function (e) {
      var offset = $('.panel.panel-default > .panel-collapse.in').offset();
      if(offset) {
          $('html,body').animate({
              scrollTop: $('.panel-collapse.in').siblings('.panel-heading').offset().top - 60
          }, 500); 
      }
  });
});

$('.box-office-tabs .panel-collapse').on('show.bs.collapse', function (e) {
  $(e.target).closest('.panel').siblings().find('.panel-collapse').collapse('hide');
});

$(function(){
	$(".uni-select select").uniform();
});


$(document).ready(function () {
  $('.tooltip-right').tooltip({
    placement: 'right',
    viewport: {selector: 'body', padding: 2}
  })
  $('.tooltip-bottom').tooltip({
    placement: 'bottom',
    viewport: {selector: 'body', padding: 2}
  })
  $('.tooltip-viewport-right').tooltip({
    placement: 'right',
    viewport: {selector: '.container-viewport', padding: 2}
  })
  $('.tooltip-viewport-bottom').tooltip({
    placement: 'bottom',
    viewport: {selector: '.container-viewport', padding: 2}
  })
});



$(".main-slider").slick({
    autoplay: true,
    autoplaySpeed: 9000,
    speed: 900,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    cssEase: 'linear',
    fade: true,
    arrows: false
});

$(".small-slider").slick({
    autoplay: true,
    autoplaySpeed: 9000,
    speed: 900,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    cssEase: 'linear',
    fade: true,
    arrows: false
});

$(document).ready(function() {
  $(".film-slider").owlCarousel({
    autoPlay: true,
    pagination: false,
    loop: true,
    margin: 0,
    nav: true,
    autoplay: true,
    items: 1,
    navText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
    ],
    responsive : {
        400 : {
           items: 2
        },
        581 : {
            items: 3
        },
        992 : {
            items: 4
        },
        1200 : {
            items: 5
        }
      }
  });
});

/*
$(".film-slider").slick({
    autoplay: true,
    autoplaySpeed: 9000,
    speed: 900,
    slidesToShow: 5,
    slidesToScroll: 5,
    dots: false,
    arrows: true,
    prevArrow: '<i class="fa fa-angle-left"></i>',
    nextArrow: '<i class="fa fa-angle-right"></i>',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 580,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
});
*/

if (/iP(hone|od|ad)/.test(navigator.platform)) {
  $(".main-films .film-slider .cols figure").css({"cursor":"pointer"});
};

$('.tooltipster').tooltipster({
    functionInit: function(instance, helper){

        var $origin = $(helper.origin),
            dataOptions = $origin.attr('data-tooltipster');

        if(dataOptions){
      
            dataOptions = JSON.parse(dataOptions);

            $.each(dataOptions, function(name, option){
                instance.option(name, option);
            });
        }
    },
    animation: 'fade',
    delay: 200,
    animationDuration: 300,
    contentCloning: true,
    multiple: true,
    theme: 'tooltipster-borderless',
    interactive: 'true',
    minWidth: 250,
    trigger: 'custom',
    triggerOpen: { mouseenter: true, touchstart: true },
    triggerClose: { mouseleave: true, tap: true },
});


/* registration */
$(".btn-registration").on("click", function(e){
  $(".layer.registration").toggleClass('active');
  $(".layer-scroll").toggleClass('active');
  $(".hsearch-cont").removeClass('active');
  $(".layer.login").removeClass('active');
  e.stopPropagation();
});
$(".layer.registration").on("click touchstart", function(e){
  e.stopPropagation();
});
$(".layer-scroll").on("click", function(e){
  $(".layer.registration").removeClass('active');
});
$(document).on("click", function(){
  $(".layer.registration").removeClass('active');
  $(".layer-scroll").removeClass('active');
});
$(".layer.registration .fa-times-circle-o").on("click", function(e){
  $(".layer.registration").removeClass('active');
  $(".layer-scroll").removeClass('active');
  e.stopPropagation();
});


/* login */
$(".btn-login").on("click", function(e){
  $(".layer.login").toggleClass('active');
  $(".layer-scroll2").toggleClass('active');
  $(".layer.registration").removeClass('active');
  $(".hsearch-cont").removeClass('active');
  e.stopPropagation();
});
$(".layer.login").on("click touchstart", function(e){
  e.stopPropagation();
});
$(".layer-scroll2").on("click", function(e){
  $(".layer.login").removeClass('active');
});
$(document).on("click", function(){
  $(".layer.login").removeClass('active');
  $(".layer-scroll2").removeClass('active');
});
$(".layer.login .fa-times-circle-o").on("click", function(e){
  $(".layer.login").removeClass('active');
  $(".layer-scroll2").removeClass('active');
  e.stopPropagation();
});
$('.lstpwd-btn').click(function(e) {
  e.preventDefault();
  $('.lstpwd').slideToggle();
  $('.lstpwd-btn').toggleClass('active');
});
$('.lstpwd-cancel-btn').click(function(e) {
  e.preventDefault();
  $('.lstpwd').slideToggle();
  $('.lstpwd-btn').toggleClass('active');
});


/* hsearch */
$(".btn-hsearch").on("click", function(e){
  $(".hsearch-cont").toggleClass('active');
  $(".layer.registration").removeClass('active');
  $(".layer.login").removeClass('active');
  e.stopPropagation();
});
$(".hsearch-cont").on("click touchstart", function(e){
  e.stopPropagation();
});
$(document).on("click", function(){
  $(".hsearch-cont").removeClass('active');
});


/* mobil menu */
$('.btn-mobile-menu').click(function(e) {
  e.preventDefault();
  $('body').toggleClass('smenu');
  $('.btn-mobile-menu').toggleClass('collapsed');
});
$('.gray-overlay').click(function(e) {
  e.preventDefault();
  $('body').toggleClass('smenu');
  $('.btn-mobile-menu').toggleClass('collapsed');
});


/* addcomment-btn */
$('.addcomment-btn').click(function(e) {
  e.preventDefault();
  id = this.id;
  console.log(id);
  $('.addcomment.item_'+id).slideToggle();
});



$(document).ready(function() {
  $(".movies-carousel").owlCarousel({
    items: 3,
    autoPlay: true,
    pagination: false,
    loop: true,
    margin: 7,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
    ]
  });
});


var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};

$('body').popover({
  selector: '[data-popover]',
  trigger: 'hover',
  placement: 'auto bottom',
  delay: {
    show: 50,
    hide: 50
  }
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

jQuery(document).ready(function($) {
  $('a[data-rel^=lightcase]').lightcase({
    swipe: true,
    maxWidth: '100%',
    showSequenceInfo: false,
  });
});