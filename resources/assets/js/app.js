
$(document).ready(function () {    

    if ($("#aszf").is(":checked")) {
        $('#saveStatusButton').show();
    } else {
        $('#saveStatusButton').hide();
    }


    /// Check notifications in every 10 sec
    var inter = 10000;

    setInterval(function () {

        $.ajax({
            method: "GET",
            url: "/getnotifications"
        })
                .done(function (msg) {

                    if (msg == '0') {
                        //setNotification('Új hozzászólásod érkezett!');
                        console.log('There is no new notification.');
                    }
                    else{
                        setNotification(msg);
                    }
                });
    }, inter);


    $("#registrationButton").on('click', function () {

        var erRor = 0;
        var error_message = 0;

        var name = $("#name").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var email = $("#email").val()
        var password_confirm = $("#password-confirm").val();
        var mypassword = $("#password").val();

        if (!email) {
            email = "fake";
        }

        $.ajax({
            method: "GET",
            url: "/check_user_email/" + email
        })
                .done(function (msg) {

                    if (msg == 1) {
                        error_message = 'A megadott email cím már használatban van!';
                        erRor = 8;
                    }

                    if (password_confirm !== mypassword) {
                        error_message = 'A megadott jelszavak nem egyeznek meg!';
                        erRor = 7;
                    }
                    if (!password_confirm) {
                        error_message = 'Töltse ki a jelszó megerősítése mezőt is!';
                        erRor = 6;
                    }
                    if (!mypassword) {
                        error_message = 'Töltse ki a jelszó mezőt is!';
                        erRor = 5;
                    }
                    if (!lastname) {
                        error_message = 'Töltse ki a keresztnév mezőt is!';
                        erRor = 4;
                    }
                    if (!firstname) {
                        error_message = 'Töltse ki a vezetéknév mezőt is!';
                        erRor = 3;
                    }
                    if (!email || email == 'fake') {
                        error_message = 'Töltse ki az email cím mezőt is!';
                        erRor = 2;
                    }
                    if (!name) {
                        error_message = 'Töltse ki meg a felhasználónév mezőt is!';
                        erRor = 1;
                    }

                    //check error message and submit form
                    if (erRor == 0) {
                        message = 'Sikeres regisztráció! Beléptetés folyamatban...';
                        $("#displayFrameErrors").html('<div class="alert alert-success">'+message+'</div>');
                        $("#registrationForm").submit();
                    } else {
                        ermsg = '<div class="alert alert-danger">' + error_message + '</div>';
                        $("#displayFrameErrors").html(ermsg);
                    }

                });
    });




});
///End of onload 

$(window).load(function () {
    $('#webApp').fadeIn();
    $('#basicModal').modal('show');
});

// collapse close opened item
$(document).on('click', '.navbar-collapse.in', function (e) {
    if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
        $(this).collapse('hide');
    }
});

function setNotification(text) {

    $.notify(text, {
        // whether to hide the notification on click
        clickToHide: true,
        // whether to auto-hide the notification
        autoHide: true,
        // if autoHide, hide after milliseconds
        autoHideDelay: 12000,
        // show the arrow pointing at the element
        arrowShow: true,
        // arrow size in pixels
        arrowSize: 5,
        // position defines the notification position though uses the defaults below
        position: '...',
        // default positions
        elementPosition: 'bottom left',
        globalPosition: 'bottom right',
        // default style
        style: 'bootstrap',
        // default class (string or [string])
        className: 'success',
        // show animation
        showAnimation: 'slideDown',
        // show animation duration
        showDuration: 300,
        // hide animation
        hideAnimation: 'slideUp',
        // hide animation duration
        hideDuration: 200,
        // padding between element and notification
        gap: 2
    });
}

function setMessage(text) {

    $.notify(text, {
        // whether to hide the notification on click
        clickToHide: true,
        // whether to auto-hide the notification
        autoHide: true,
        // if autoHide, hide after milliseconds
        autoHideDelay: 12000,
        // show the arrow pointing at the element
        arrowShow: true,
        // arrow size in pixels
        arrowSize: 5,
        // position defines the notification position though uses the defaults below
        position: '...',
        // default positions
        elementPosition: 'bottom right',
        globalPosition: 'bottom right',
        // default style
        style: 'bootstrap',
        // default class (string or [string])
        className: 'info',
        // show animation
        showAnimation: 'slideDown',
        // show animation duration
        showDuration: 300,
        // hide animation
        hideAnimation: 'slideUp',
        // hide animation duration
        hideDuration: 200,
        // padding between element and notification
        gap: 2
    });
}

function likeIt(id, contentType, todo, likeID) {
    
    $.ajax({
        method: "GET",
        url: "/setforumlike",
        data: {contentType: contentType, itemid: id, todo: todo, likeID: likeID}
    })
            .done(function (msg) {
                
                if(msg==100){
                    setMessage('A funkció használatához be kell jelentkezned!');                    
                }                
              
                if(msg==110){
                    setMessage('A dislike bejegyezve!');
                    $(".dislike_id_"+id).html(parseInt($(".dislike_id_"+id).html())+1);
                }
                
                if(msg==120){
                    setMessage('A dislike-ot visszavontad!');
                    $(".dislike_id_"+id).html(parseInt($(".dislike_id_"+id).html())-1);
                }
                
                if(msg==130){
                    setMessage('A like bejegyezve!');
                    $(".like_id_"+id).html(parseInt($(".like_id_"+id).html())+1);
                }                
                
                if(msg==140){
                    setMessage('A like-ot visszavontad!');
                    $(".like_id_"+id).html(parseInt($(".like_id_"+id).html())-1);
                }
                
                if(msg==150){
                    setMessage('A like-ot módosítottad!');
                    $(".like_id_"+id).html(parseInt($(".like_id_"+id).html())+1);
                    $(".dislike_id_"+id).html(parseInt($(".dislike_id_"+id).html())-1);
                }                
                
                if(msg==160){
                    setMessage('A dislike-ot módosítottad!');
                    $(".like_id_"+id).html(parseInt($(".like_id_"+id).html())-1);
                    $(".dislike_id_"+id).html(parseInt($(".dislike_id_"+id).html())+1);
                }                
 
                
            });
}


$("#critics_lead").keyup(function () {
    var maximum = 300;
    var recent_text_amount = $("#critics_lead").val();
    var length = recent_text_amount.length;
    var remain = parseInt(maximum) - parseInt(length);
    $("#lead_text_counter").val(remain);

    if (length > 299) {
        var shortText = jQuery.trim(recent_text_amount).substring(0, 300).trim(this);
        $("#critics_lead").val(shortText);
        $("#lead_text_counter").val(0);
    }

});

$("#introduction").keyup(function () {
    var maximum = 500;
    var recent_text_amount = $("#introduction").val();
    var length = recent_text_amount.length;
    var remain = parseInt(maximum) - parseInt(length);
    $("#text_counter").val(remain);

    if (length > 499) {
        var shortText = jQuery.trim(recent_text_amount).substring(0, 500).trim(this);
        $("#introduction").val(shortText);
        $("#text_counter").val(0);
    }

});


function reorderNews(where) {
    //redirect with reorder parameter
    ertek = $("#" + where.id).val();
    window.location.replace("/hirek/?rendezes=" + ertek);

}

function reorderList(where, link) {
    //redirect with reorder parameter
    ertek = where.value;
    serach = $("#searchFilm").val();
    window.location.replace("/"+link+"/?rendezes=" + ertek+'&searchFilm='+serach);
}

function reorderCritics(where) {
    //redirect with reorder parameter
    ertek = $("#" + where.id).val();
    window.location.replace("/kritikak/?rendezes=" + ertek);
}

function reorderAuthorCritics(val, gothere) {
    //redirect with reorder parameter    
    window.location.replace("/szerzok/"+gothere+"/?rendezes=" + val+"#linkToThis");
}

function reorderCriticsByName(where) {
    //redirect with reorder parameter
    ertek = $("#" + where.id).val();
    window.location.replace("/kritikak/?lista=" + ertek);
}

function reorderProfileCritics(value, type) {
    window.location.replace("/profil/adatlap/?lista="+value+"&type="+type);
}


$("#aszf").on('click', function () {

    if ($("#aszf").is(":checked")) {
        //console.log('csekked');
        $('#saveStatusButton').show();
    } else {
        //console.log('nem csekked');
        $('#saveStatusButton').hide();
    }
});


$("#publish").on('click', function () {

    var title = $("#title").val();
    var lead = $("#lead").val();
    var film = $("#filmID").val();
    var imdb = $("#imdb").val();
    var rate = $("#ex19").val();
    var description = $("#description").val();


    $("#form_critics").submit();

});


$('#ex19').change(function (e) {
    sendFilmRate(this.value, $("#filmID").val());
});

function sendFilmRate(myval, filmID) {

    $.ajax({
        method: "get",
        url: "/setfilmrate",
        data: {myval: myval, filmID: filmID}
    })
            .done(function (msg) {
                //done
                console.log(msg);
            });

}

$("#loginButton").on('click', function () {    
    loginFunction();
});

function loginFunction(){
    
    email = $("#loginemail").val();
    pw = $("#loginpassword").val();
    
    
        $.ajax({
        method: "get",
        url: "/checklogin",
        data: {loginemail: email, loginpassword: pw}
    })
            .done(function (msg) {
                //done                
                if(msg==0){
                    msg = "<span style='color: #ff0000;'>Houston, van egy kis gondunk! Az email cím vagy jelszó hibás!</span>";
                    $("#loginErrorMsg").html(msg);
                }
                
                if(msg==1){
                    msg = "<span style='color: green;'>Sikeres bejelentkezés!</span>";
                    $("#loginErrorMsg").html(msg);
                    $( "#loginForm" ).submit();
                }

            });    
}



function report(id, user_id, type){    
    
        $.ajax({
        method: "get",
        url: "/submitreport",
        data: {id: id, user_id: user_id, type: type}
    })
            .done(function (msg) {
                //done                               
                if(msg==0){
                    setMessage('A bejelentést rögzítettük!');
                }
                else{
                    setMessage('A bejelentést korábban már rögzítettük!');
                }

            });    
}
                            
           
function loadBoxOfficeData(ho, ev, nap) {

    $.ajax({
        method: "get",
        url: "/box-office/getdata",
        data: {ho: ho,ev: ev,nap: nap}
    })
            .done(function (msg) {
                //done
                $("#boxOfficeFrame").html(msg);
            });

}

function loadFilmData(link, datum) {
    window.location.replace("/"+link+"/?datum="+datum+"");  
}

function checkDataType(link){
 
    if($('#cmn-toggle-1').is(':checked')){
        window.location.replace("/"+link+"/?view=week");
    }    
    else{
        window.location.replace("/"+link+"/?view=weekend");   
    }    
    
}

function checkDataTypeFilm(filmid, category){
 
    if($('#cmn-toggle-1').is(':checked')){
        window.location.replace("/film/"+filmid+"/"+category+"/?view=week");
    }    
    else{
        window.location.replace("/film/"+filmid+"/"+category+"/?view=weekend");   
    }    
    
}

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#temphide').hide();
                $('#tempimg').show();
                $('#tempimg')
                    .attr('src', e.target.result)
                    .width(252);
            };

            reader.readAsDataURL(input.files[0]);
        }
}


$("#createTopik").on('click', function () {
    
   if($("#create-topic-form .title").val()!=="" && $("#create-topic-form .description").val()!==""){
       setNotification('Köszönjük! Sikeresen beküldted a topic-ot, amely adminisztrátori jóváhagyás után fog megjelenni.');
   }
    
});