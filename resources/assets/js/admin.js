$(document).ready(function () {

    $('#mainContent').fadeIn();

    $('.wisywigEditor').summernote({
        height: 300, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: true
    });

    $('#datatableFilms').DataTable({
        "initComplete": function (settings, json) {
            $("tbody").slideDown();
        },
        "language": {
            "lengthMenu": "_MENU_ sor megjelenítése oldalanként",
            "zeroRecords": "Nincs találtat!",
            "info": "_PAGE_ / _PAGES_ oldal",
            "infoEmpty": "No records available",
            "search": "Keresés:",
            "infoFiltered": "(_MAX_ bejegyzésből szűrt eredmény)",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            }
        },
        "aoColumns": [
            {"bSearchable": false, "bSortable": true},
            {"bSearchable": true, "bSortable": true},
            {"bSearchable": false, "bSortable": true},
            {"bSearchable": false, "bSortable": false},
            {"bSearchable": false, "bSortable": false}
        ]
    });

    $('#datatable').DataTable({
        "initComplete": function (settings, json) {
            $("tbody").slideDown();
        },
        "language": {
            "lengthMenu": "_MENU_ sor megjelenítése oldalanként",
            "zeroRecords": "Nincs találtat!",
            "info": "_PAGE_ / _PAGES_ oldal",
            "infoEmpty": "No records available",
            "search": "Keresés:",
            "infoFiltered": "(_MAX_ bejegyzésből szűrt eredmény)",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            }
        },
        "aoColumns": [
            {"bSearchable": false, "bSortable": true},
            {"bSearchable": true, "bSortable": true},
            {"bSearchable": false, "bSortable": false},
            {"bSearchable": false, "bSortable": false}
        ]
    });

    $('#datatableBannerStat').DataTable({
        "language": {
            "lengthMenu": "_MENU_ sor megjelenítése oldalanként",
            "zeroRecords": "Nincs találtat!",
            "info": "_PAGE_ / _PAGES_ oldal",
            "infoEmpty": "No records available",
            "search": "Keresés:",
            "infoFiltered": "(_MAX_ bejegyzésből szűrt eredmény)",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            }
        }
    });

     $('#datatableArticles').DataTable({
        "initComplete": function (settings, json) {
            $("tbody").slideDown();
        },
        "language": {
            "lengthMenu": "_MENU_ sor megjelenítése oldalanként",
            "zeroRecords": "Nincs találtat!",
            "info": "_PAGE_ / _PAGES_ oldal",
            "infoEmpty": "No records available",
            "search": "Keresés:",
            "infoFiltered": "(_MAX_ bejegyzésből szűrt eredmény)",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            }
        },
        "aoColumns": [
            {"bSearchable": true, "bSortable": true},
            {"bSearchable": false, "bSortable": false},
            {"bSearchable": false, "bSortable": false},
            {"bSearchable": false, "bSortable": false}
        ]
    });
    
    $('#datatableBanner').DataTable({
        "language": {
            "lengthMenu": "_MENU_ sor megjelenítése oldalanként",
            "zeroRecords": "Nincs találtat!",
            "info": "_PAGE_ / _PAGES_ oldal",
            "infoEmpty": "No records available",
            "search": "Keresés:",
            "infoFiltered": "(_MAX_ bejegyzésből szűrt eredmény)",
            "paginate": {
                "first": "Első",
                "last": "Utolsó",
                "next": "Következő",
                "previous": "Előző"
            }
        }
    });





    var pageids = '';
    var dat = "";

    $("#assignButton").click(function () {
        
        //read item nume
        addedNum = $("#pageIDs").val();
        
        //increase item num
        $("#pageIDs").val(parseInt(addedNum)+1);
        

        //get data
        pageID = $("#assigned").val();
        bannerTitle = $("#title").val();
        pageText = $("#assigned option[value='" + pageID + "']").text();
        bannerActiveFrom = $("#validFrom").val();
        bannerActiveUntil = $("#validTo").val();

        $("#assigned option[value='" + pageID + "']").remove();

        bannerID = $("#bannerID").val();

        if (pageID) {
            //create output 	
            data = "<div class='generatedItems'>A banner megjelenik a(z) <b>'" + pageText + "'</b> oldalon <br>Egyedi megjelenési időszak megadása: <input type='hidden' name='page_"+addedNum+"' id='page_"+addedNum+"' value='"+pageID+"' /><input min='"+bannerActiveFrom+"' type='date' name='from_"+addedNum+"' id='from_"+addedNum+"' value=''> - <input min='"+bannerActiveFrom+"' type='date' name='to_"+addedNum+"' id='to_"+addedNum+"' value=''></div>";
            //display the output
            $("#choosen").append(data);
            //push new item to the array


            $("#assigned").show();
            $("#assignButton").show();
        } else {

            $("#assigned").hide();
            $("#assignButton").hide();
            alert('Nincs több hozzárendelhető oldal!');

        }
        

    });

});

$(function () {
    var match = document.cookie.match(new RegExp('color=([^;]+)'));
    if (match)
        var color = match[1];
    if (color) {
        $('body').removeClass(function (index, css) {
            return (css.match(/\btheme-\S+/g) || []).join(' ')
        })
        $('body').addClass('theme-' + color);
    }

    $('[data-popover="true"]').popover({html: true});

});

$(function () {
    var uls = $('.sidebar-nav > ul > *').clone();
    uls.addClass('visible-xs');
    $('#main-menu').append(uls.clone());
});


function confDelete(del) {
    if (confirm("Biztosan törölni szeretnéd?") == true) {
        window.location.assign("/admin/" + del);
    }
}

function setOrder(data) {

    url = window.location.href;
    chk = url.split('?');

    chkSort = url.split('sort=');

    if (chk.length > 1) {
        if (chkSort.length > 1) {
            window.location.assign(chkSort[0] + "sort=" + data);
        } else {
            window.location.assign(url + "&sort=" + data);
        }

    } else {
        window.location.assign(url + "?sort=" + data);
    }
}


function addStabItem(role, code, val){
    
    var d = new Date();
    var code = d.getTime();
    
    var name = $("#"+role).val();
    data = "<div id='id_"+code+"' class='temp-item'><input type='text' id='"+code+"' value='"+name+"' readonly><i onclick='deleteStabItem("+code+", "+role+")' class='glyphicon glyphicon-remove delete-temp-stab-item'></i></div>";
    
    if(name.length>2){        
        $("#dyn_"+role).append(data);
        $("#"+role).val("");                 
        rv = $("#"+role+"Values").val();       
        $("#"+role+"Values").val(rv+'::'+name);                
    }
}

                
function deleteStabItem(item, role){       
    
    dta = $("#"+role.id+"Values").val();  
    datarray = dta.split("::");

    var name = $("#"+item).val();
    
    var indexe = datarray.indexOf(name);
    datarray.splice(indexe, 1);
    
    newItems = datarray.join('::');
    
    $("#"+role.id+"Values").val(newItems); 
    
    console.log(datarray);
    $("#id_"+item).remove();
    
}     

    $("#checkAll").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    
    
    
     




function deleteitem(id){
    
    token = $("#_token").val();
    
    $.ajax({
      method: "POST",
      url: "/admin/banners/deleteitem",
      data: {_token:token, id: id }
    })
      .done(function( msg ) {
          alert('Az oldal törölve!');
      });    
}