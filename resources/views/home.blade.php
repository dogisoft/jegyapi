@include('blocks/header') 
@include('blocks/menu')

<?php

use App\Http\Controllers\HomeController; ?>

<div class="container-fluid">         
    <div class="col-md-12" style="margin-top:4px;">
        <h1>PROGRAMOK</h1> 
    </div>
                
    <div class="col-md-8" style="margin-top:4px;">
        
        ESEMÉNYKERESŐ		
        <div class="row">
            <form id="formSubmit" action="/filter" method="get">

                <div class="col-md-3">
                    <div class="btn-group bootstrap-">                            
                        <select class="form-control" name="cat" id="cat" onchange="updateCities(this)">
                            <option value="0">Összes kategória</option>        
                            @foreach($category as $category_item)                                    
                            <option value="{{$category_item->jid}}">{{$category_item->name}}</option>
                                @if(HomeController::getChild($category_item->jid))
                                    @foreach(HomeController::getChild($category_item->jid) as $c_item)                                        
                                        <option value="sub_cat_id_{{$c_item->jid}}"> - {{$c_item->name}}</option>                                                                        
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="btn-group bootstrap">
                        <select class="form-control" name="city" id="city" onchange="updateVenues(this)">
                            <option value="0">Összes város</option>
                            @foreach($cities as $item)                                    
                            <option value="{{$item->name}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-group bootstrap-">
                        <select class="form-control" name="venue" id="venue">
                            <option value="0">Összes helyszín</option>  
                            @foreach($venues as $item)                                      
                            <option value="{{$item->name_url}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="col-md-3">
                    <button onclick="showResults();" type="button" class="btn btn-primary">mutasd</button>
                </div>
            </form>      
        </div>
    </div>
    <div class="col-md-4" style="margin-top:15px;">
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <form action="/keres" method="get" id="search">
                    <input type="text" class="form-control input-lg" placeholder="keresés" name="kereses">
                </form>

                <span class="input-group-btn">
                    <button class="btn btn-info btn-lg" type="button" onclick="submitForm();">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>


    <div class="col-md-8">
        <b>Összesen:</b> {{$eventnum}}  esemény 
        
        <br>@if(isset($res)) <b>Találatok száma: </b> {{$res}} @endif
        <br>
        <!--body-->

        <div class="row">
            <?php $itt = array(); ?>
            
            @foreach($events as $event)
            
            <?php            
                 
                if(in_array($event->NetProgram_Id, $itt)){
                    
                }
                else{
                    array_push($itt, $event->NetProgram_Id);
            ?>
            
            <div class="col-md-4" id="hover" style="height: 340px; margin: 25px 0 0 0;">
                <a href="/esemeny/{{$event->NameURL}}/{{$event->id}}" class="hvr-underline-from-left">
                    @if($event->ThumbURL=="")
                    <img src="/img/no-image.jpg" style="width:100%"><br>
                    @else
                    <img src="{{$event->ThumbURL}}" style="width:100%"><br>
                    @endif
                    <h2>{{$event->ProgramName}}</h2>
                    <div class="helyszin">{{$event->VenueName}}</div>
                    <div class="datum"><span class="glyphicon glyphicon-calendar" style="color:#8c114c;" aria-hidden="true"></span> {{date('Y-m-d H:i', $event->EventDateTS)}}</div>
                </a>
            </div>
                <?php } ?>
            @endforeach
        </div>
        @if(Request::segment(1)=="keres")
            {{ $events->appends(['kereses' => $kereses])->links() }}
        @else
            @if(isset($cat) || isset($city) || isset($venue))
                {{ $events->appends(['cat' => $cat, 'city' => $city, 'venue' => $venue])->links() }}
            @else
                {{ $events->links() }}
            @endif
        @endif
        

       
        <!--body-->
    </div>
    <div class="col-md-4">
        <!--right-->
        <div class="row">
            @include('blocks/ajanlo')
        </div>
        <!--right-->
    </div>
</div>
    
 

<script>
   
    
    function submitForm() {
        $("#search").submit();
    }
</script>

@include('blocks/footer') 