<!DOCTYPE html>
<!-- saved from url=(0033)http://programguru.hu/programok/# -->
<html lang="hu"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="@if(isset($eventData->ProgramName)){{$eventData->ProgramName}}@endif">
        <meta name="keyword" content="@if(isset($eventData->ProgramName)){{$eventData->ProgramName}}@endif">

        <title>Programguru.hu @if(isset($eventData->ProgramName)) - {!!$eventData->ProgramName!!}@endif</title>

        <!-- Bootstrap core CSS -->

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/bootstrap-select.css">

        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap-select.js"></script>
        <script src="/js/app.js"></script>


        <!--link href="css/demo-page.css" rel="stylesheet" media="all"-->
        <link href="/css/hover.css" rel="stylesheet" media="all">
        <!--link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all"-->

        <style>		
            <!--
body {
margin:0px;
padding:0px;
}			
   .top1 {
        float: left;
        width: 20%;
    }
    .top2 {
        float: right;
        width: 80%;
		margin-top:15px;
		
    }
	
    img {
        max-width: 100%;
        height: auto;
    }
    .group:after {
        content:"";
        display: table;
        clear: both;
		
    }
   /* @media screen and (max-width: 680px) {
        .top1, .top2 {
            float: none;
            width: auto;
        }
    }*/
    @media screen and (max-width: 590px) {
        .top1 {
            float: none;
            width: auto;
        }
    }

ul.topnav {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;

}

ul.topnav li {float: left;}

ul.topnav li a {
  display: inline-block;
  color: #FFFFFF;
  text-align: center;
  padding: 14px 8px;
  text-decoration: none;
  transition: 0.3s;
  font-size: 14px;
font-family: 'Raleway', sans-serif;
}

ul.topnav li a:hover {background-color: #555;}

ul.topnav li.icon {display: none;}

@media screen and (max-width:1280px) {
  ul.topnav li:not(:first-child) {display: none;}
  ul.topnav li.icon {
    float: right;
    display: inline-block;
  }
}

@media screen and (max-width:1280px) {
  ul.topnav.responsive {position: relative;}
  ul.topnav.responsive li.icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  ul.topnav.responsive li {
    float: none;
    display: inline;
  }
  ul.topnav.responsive li a {
    display: block;
    text-align: left;
  }
}	

#custom-search-input{
    padding: 3px;
    border: solid 1px #E4E4E4;
    border-radius: 6px;
    background-color: #fff;
}

#custom-search-input input{
    border: 0;
    box-shadow: none;
}

#custom-search-input button{
    margin: 2px 0 0 0;
    background: none;
    box-shadow: none;
    border: 0;
    color: #666666;
    padding: 0 8px 0 10px;
    border-left: solid 1px #ccc;
}

#custom-search-input button:hover{
    border: 0;
    box-shadow: none;
    border-left: solid 1px #ccc;
}

#custom-search-input .glyphicon-search{
    font-size: 23px;
}
h2 {
    font-size: 16px;
	margin-top: 2px;
	font-weight:600;
}
.datum {
    font-size: 12px;
	margin-bottom: 10px;
	
}
#hover a {
text-decoration:none;
color:#000000;
}

#hover a:hover {
text-decoration:none;
color:#8c114c;
}
h1 {
font-family: 'Abel', sans-serif;
font-size:40px;
}


            -->	

        </style>	

<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Abel|Fjalla+One" rel="stylesheet">

</head>
<body>