        <div style="clear:both;"></div>

        <!--footer-->
        <div style="text-align:center;height:200px;margin-top:100px;background-color: #626c73;">
            <br><br>
            <a href="http://programguru.hu/impresszum.html" style="color:#ffffff;">Impresszum - Kapcsolat</a> | <a href="http://programguru.hu/adatvedelmi-tajekoztato" style="color:#ffffff;">Adatvédelmi tájékoztató</a>
        </div>
        <!--footer-->

        <script>
            /**
             * Used to demonstrate Hover.css only. Not required when adding
             * Hover.css to your own pages. Prevents a link from being
             * navigated and gaining focus.
             */
            var effects = document.querySelectorAll('.effects')[0];

            effects.addEventListener('click', function (e) {

                if (e.target.className.indexOf('hvr') > -1) {
                    e.preventDefault();
                    e.target.blur();

                }
            });
        </script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/js/ie10-viewport-bug-workaround.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/js/npm.js"></script>
        <script src="/js/bootstrap.min.js"></script>

    </body>
</html>