<div style="font-family: 'Abel', sans-serif;font-size:25px; margin: 20px 0 10px 10px;">AJÁNLÓ</div>
@foreach(App\Event::ajanlo() as $event)
<div class="col-md-12" id="hover">
    <a href="/esemeny/{{$event->NameURL}}" class="hvr-underline-from-left">
        @if($event->ThumbURL=="")
        <img src="/img/no-image.jpg" width="474" class="img-responsive"><br>
        @else
        <img src="{{$event->ThumbURL}}" width="474"><br>
        @endif
        <h2>{{$event->ProgramName}}</h2>
        <div class="helyszin">{{$event->VenueName}}</div>
        <div class="datum"><span class="glyphicon glyphicon-calendar" style="color:#8c114c;" aria-hidden="true"></span> {{$event->EventDateFormat}}</div>
    </a>
</div> <br><br>               
@endforeach
