        <div class="group" style="width:100%;background-color:#8c114c;">  
            <div class="top1">
                <a href="http://programguru.hu/"><img src="/img/gurulogo.png"></a>
            </div>
            <div class="top2">
                <ul class="topnav" id="myTopnav">
                    <li>
                        <a href="http://programguru.hu/">FŐOLDAL</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/kultur-naplo.html">KULTÚR NAPLÓ</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/kiallitas.html">KIÁLLÍTÁS</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/galeriak-vilaga.html">GALÉRIÁK VILÁGA</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/hangolo.html">HANGOLÓ</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/fesztival.html">FESZTIVÁL</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/borkultura.html">BORKULTÚRA</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/konyvajanlo.html">KÖNYVAJÁNLÓ</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/kozonseg-kavehaz.html">KÖZÖNSÉG KÁVÉHÁZ</a>
                    </li>
                    <li>
                        <a href="http://programguru.hu/video">VIDEÓ (új)</a>
                    </li>
                    <li>
                        <a href="https://programguru.jegy.hu/">JEGYVÁSÁRLÁS</a>
                    </li>



                    <li class="icon">
                        <a href="javascript:void(0);" style="font-size:30px;" onclick="myFunction()"><img src="/img/menu-top.png"></a>
                    </li>
                </ul>

                <script>
                    function myFunction() {
                        var x = document.getElementById("myTopnav");
                        if (x.className === "topnav") {
                            x.className += " responsive";
                        } else {
                            x.className = "topnav";
                        }
                    }
                </script>  
            </div>
        </div>	