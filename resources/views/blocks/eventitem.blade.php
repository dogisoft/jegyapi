<!--body-->
<div class="col-md-6">	
    <div style="font-family: 'Abel', sans-serif;font-size:25px;"><b>PROGRAM</b></div>
    <h1>{{$eventData->ProgramName}}</h1>

    <div><span class="glyphicon glyphicon-map-marker" style="color:#8c114c;font-size:25px;" aria-hidden="true"></span><a href="https://programguru.jegy.hu/venue/{{$eventData->VenueNameURL}}">{{$eventData->City}}, {{$eventData->VenueName}}</a></div>

    <div><a href="https://programguru.jegy.hu/ticketsearch/venue/{{$eventData->VenueNameURL}}">A helyszín további eseményei >></a></div>

    <br>Időpont: {{date('Y-m-d H:i', $eventData->EventDateTS)}}

</div>
<div class="col-md-6 pull-right">	
    <img src="{{$eventData->Image}}" alt="{{$eventData->ProgramName}}" title="{{$eventData->ProgramName}}" class="img-responsive" id="programMainImage" data-thumb-index="0" data-imagecount="8" />
</div>

<div class="col-md-12">
    <a href="{{$eventData->LinkURL}}"><button type="button" class="btn" style="background-color:#8c114c;color:#ffffff;font-size:25px;">jegyvásárlás</button></a>
    <br><br>
    <div style="font-family: 'Abel', sans-serif;font-size:18px;"><b>LEÍRÁS</b></div>
    <br>
    <div style="font-size:18px;">
        {{$eventData->ShortDescription}}
    </div>
    
</div>



