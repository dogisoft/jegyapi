
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jegy.hu API teszt</title>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
</head>

<body>
    
<h2>Válaszd ki, melyik részt szeretnéd aktualizálni.</h2>

<div><a href="/update-city">A város lista aktualizálása</div>
<div><a href="/update-category">A kategória lista aktualizálása</div>
<div><a href="/update-venue">A helyszín lista aktualizálása</div>
<div><a href="/update-event">Az esemény lista aktualizálása</div>

</body>
</html>



