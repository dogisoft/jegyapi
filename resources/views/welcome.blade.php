
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jegy.hu API teszt</title>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
</head>

<body>

<form id="form1" name="form1" method="post" action="https://programguru.jegy.hu/api">
  <p>&nbsp;</p>
  <p>
    <label for="_session_id">_session_id</label>
    <input name="_session_id" type="text" id="_session_id" value="" size="48" />
  </p>
  <p>
    <label for="ResourceType">_function_name</label> 
    <select name="_function_name" id="_function_name">
      <option value="get_event_list">get_event_list</option>
      <option value="get_event_list_count">get_event_list_count</option>
      <option value="get_event_list_simple">get_event_list_simple</option>
      <option value="get_seasonticket_list">get_seasonticket_list</option>
      <option value="get_initial_content">get_initial_content</option>
      <option value="get_normal_news">get_normal_news</option>
      <option value="get_person">get_person</option>
      <option value="get_program">get_program</option>
      <option value="get_program_list">get_program_list</option>
      <option value="get_program_simple">get_program_simple</option>
      <option value="get_sector_prices">get_sector_prices</option>
      <option value="get_slider_news">get_slider_news</option>
      <option value="get_toplist">get_toplist</option>
      <option value="get_venue">get_venue</option>
      <option value="get_venue_list">get_venue_list</option>
      <option value="rss">rss</option>
      <option value="search">search</option>
      <option value="get_capital_news">get_capital_news</option>
      <option value="get_categories">get_categories</option>
      <option value="get_city_list">get_city_list</option>	  
    </select>
  </p>
  <p>
    <label for="_version">_version</label>
    <input name="_version" type="text" id="_version" value="2" />
  </p>
  <p>_format 
     <select name="_format" id="_format">
      <option value="print_r">print_r</option>
      <option value="json">json</option>
      <option value="xml">xml</option>
    </select>
  </p>
  <p>
    <label for="_api_key">_api_key</label>
    <input name="_api_key" type="text" id="_api_key" size="42"></input>
  </p>
  <p>
    <label for="_request">_request<br />
    </label>
    <textarea name="_request" id="_request" cols="80" rows="20"></textarea>
  </p>
  <p>
    <label for="submit"></label>
    <input type="submit" name="submit" id="submit" />
  </p>

</form>
</body>
</html>



