@include('blocks/header') 
@include('blocks/menu')
<div class="container-fluid">
        <div style="clear:both;height:20px;"></div>

        <div style="width:100%;">                 
            <div class="col-md-8">                                     
                @include('blocks/eventitem')                                      
            </div>
                        
            <div class="col-md-4">
                <!--right-->
                <div class="row">
                    @include('blocks/ajanlo')
                </div>
                <!--right-->
            </div>
        </div>
</div>

@include('blocks/footer') 