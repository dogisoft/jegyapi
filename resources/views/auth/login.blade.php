@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div>
                        <div class="layer registration">
                            <i class="fa fa-times-circle-o visible-xs"></i>
                            <div class="ptitle orange-line text-center">
                                <span>Belépés</span>
                            </div>

                            {{ Form::open(array('url' => 'login')) }}


                           <!-- if there are login errors, show them here -->
                           <p>
                               {{ $errors->first('email') }}
                               {{ $errors->first('password') }}
                           </p>

                           <p>
                               {{ Form::label('email', 'Email Address') }}
                               {{ Form::text('email', null, array('placeholder' => 'awesome@awesome.com')) }}
                           </p>

                           <p>
                               {{ Form::label('password', 'Password') }}
                               {{ Form::password('password') }}
                           </p>

                           <p>{{ Form::submit('Submit!') }}</p>
                           {{ Form::close() }}                           
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection
