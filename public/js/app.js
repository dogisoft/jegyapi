function updateCities(item){    
    $.ajax({
      method: "get",
      url: "/get-cities-by-id/"+item.value,
    })
      .done(function( msg ) {
        $("#city").html(msg);
      });    
}

function updateVenues(item){
    
    catid = $("#cat").val();
    
    $.ajax({
      method: "get",
      url: "/get-venues/",
      data: { citytid: item.value, catid: catid }
    })
      .done(function( msg ) {
        console.log(msg);  
        $("#venue").html(msg);
      });        
}

function showResults(){
    
    var cat = $("#cat").val();
    var city = $("#city").val();
    var venue = $("#venue").val();
    
    $("#formSubmit").submit();
}